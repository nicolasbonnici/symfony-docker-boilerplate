# symfony-docker-boilerplate

Dockerized Symfony boilerplate

Support

- [x] PHP 8.3
- [x] Symfony 6.4 LTS
- [x] PostgreSQL 16
- [x] Redis
- [x] nginx http server

## Getting started

Install Docker and php locally, copy `symfony/.env.dist` to `symfony/.env` and edit to your needs, then simply run:

```bash
composer run setup
```

### More information about this project

See the [associated article on my dev.to blog](https://dev.to/nicolasbonnici/dockerized-symfony-64-project-boilerplate-3629)
